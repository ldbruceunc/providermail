**********************************************************
Provider Mail Survey - Operations pull

Requestor: Marisa Domino

Programmer: LDBruce
Last update: 16MAR2021

Notes from call 15MAR2021
Goal is to send out a mailed paper survey of Medicaid satisfaction

Providers of primary care, OB, or multi-specialty practice

Full population of organizations that meet the criteria, so that a subpopulation can be pulled

File comes from multiple data sources:
�	IQVIA file tells what organizations/practices providers belong to
�	Our Medicaid provider file has NPIs who contract with Medicaid, and which ones have billed Medicaid. 

Billing level NPI in our file is very high level, so we need the IQVIA data

Stephanie has created # unique patients per NPI through 2019, but we don�t need # patients, no PHI

NPES has several taxonomies, Medicaid has only one taxonomy

Only keep records that appear in both our data and IQVIA data

PRVDR_NTWRK_END_DT >= Jan 1, 2019 (still active contract is a high date)

Taxonomies and Org address from IQVIA

Haven�t yet rcd IQVIA data
**********************************************************
;

libname fivep "/local/projects/medicaid_testsample/Data/";
libname raw "/nearline/data/master/NCDMA_SVC_Master/";
libname outdata "/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ProviderMail/providermail/data/";

%let run_dt = %sysfunc(putn(%sysfunc(date()),MMDDYY10.));

*%include "/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ProviderMail/providermail/importcsv.sas";


ods html5 path="/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ProviderMail/providermail/" file="Prov_taxon.html";
title 'Provider taxonomy file';
title2 'NPIs from Sheps Medicaid data with taxonomy matching list from Burcu and PRVDR_NTWRK_END_DT >= 2019';
title3 "Code file: /n2/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ProviderMail/providermail/getdat.sas";  
title4 "Git repository: https://lbruceUNC@bitbucket.org/lbruceUNC/providermail.git";
title5 "Last update: &run_dt.";

proc sql;
create table outdata.pr3 as
select distinct a.npi, a.TXN_CD, PRVDR_NTWRK_END_DT, b.*
from raw.unc_provider_jan2021 (where = (year(PRVDR_NTWRK_END_DT) >= 2019)) a
inner join provflags (rename=(npi=TXN_CD)) b on a.TXN_CD = b.TXN_CD
group by 1
order by 1
;
quit;

proc contents data= outdata.pr3;
run;

title "10 sample records";
proc print data=outdata.pr3 (obs=10);
run;

ods html5 close;




